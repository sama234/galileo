<?php

/**
 * @file
 * Implementation to display a news node.
 */
?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if (isset($content['field_news_feat_img'])) : ?>
    <div class="news__featured-image">
      <?php print theme('image_style', array('style_name' => 'news_feature_header', 'path' => $node->field_news_feat_img['und'][0]['uri'])); ?>
    </div>
  <?php endif; ?>

  <p class="news__post-date"><?php print format_date($node->created, 'custom', 'F j, Y'); ?></p>
  <h1 class="news__news-title"><?php print $node->title; ?></h1>
  <?php print render($content['field_content']); ?>
</article>
