<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php
  $question = $content['field_question']['#items']['0']['value'];
  $answer = $content['field_answer']['#items']['0']['value'];
  $uid = uniqid();
?>

<div class="panel">
  <div class="panel-heading" role="tab" id="heading<?php print $uid; ?>">
    <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php print $uid; ?>" aria-expanded="false" aria-controls="collapse<?php print $uid; ?>"><?php print $question; ?> <i class="fa fa-caret-down" aria-hidden="true"></i></a></h4>
  </div>
  <div id="collapse<?php print $uid; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php print $uid; ?>">
    <div class="panel-body"><?php print $answer; ?></div>
  </div>
</div>
