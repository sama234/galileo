<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!empty($content['field_caption'])) { ?>
  <div class="content margin-30-0"<?php print $content_attributes; ?>>
  <?php } else { ?>
  <div class="content"<?php print $content_attributes; ?>>
  <?php } ?>
    <?php
    if (!empty($content['field_content_image'])) {
      $url = file_create_url($content['field_content_image']['#items']['0']['uri']);
      $link = '';
      $alt = '';
      if (!empty($content['field_content_image']['#object']->field_image_link)) {
        $link = $content['field_content_image']['#object']->field_image_link['und']['0']['value'];
      }
      if (!empty($content['field_caption'])) {
        $alt = $content['field_caption']['#items']['0']['value'];
      }
      if ($link != '') {
        print '<a href="' . $link . '"><img src="' . $url . '" class="img-responsive" alt="' . $alt . '"></a>';
      } else {
        print '<img src="' . $url . '" class="img-responsive" alt="' . $alt . '">';
      }
    }
    if (!empty($content['field_caption'])) {
      print '<div class="caption">';
      $caption = $content['field_caption']['#items']['0']['value'];
      print $caption;
      print '</div>';
    }
    ?>
  </div>
</div>
