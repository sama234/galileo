<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php
  $num = rand(1000, 10000);
  $photoID = 'photo-gallery_' . $num;
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
      $num = count($content['field_image']['#items']);
      $i = 0;
      print '<div id="' . $photoID . '" class="carousel slide" data-ride="carousel">';
        print '<ol class="carousel-indicators">';
        foreach ($content['field_image']['#items'] as $photo) {
          if ($i == 0) {
            print '<li data-target="#' . $photoID . '" data-slide-to="0" class="active"></li>';
          } else {
            print '<li data-target="#' . $photoID . '" data-slide-to="' . $i . '"></li>';
          }
          $i++;
        }
        $i = 1;
        print '</ol>';
      print '<div class="carousel-inner" role="listbox">';
      foreach ($content['field_image']['#items'] as $photo) {
        $photo_url = file_create_url($photo['uri']);
        $alt = '';
        if ($i == 1) {
          print '<div class="item active">';
          print '<img src="' . $photo_url . '" alt="' . $alt . '">';
          print '<div class="carousel-caption">' . $alt . '</div>';
          print '</div>';
        } else {
          print '<div class="item">';
          print '<img src="' . $photo_url . '" alt="' . $alt . '">';
          print '<div class="carousel-caption">' . $alt . '</div>';
          print '</div>';
        }
        $i++;
      }
      print '</div>';
      print '<a class="left carousel-control" href="#' . $photoID . '" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#' . $photoID . '" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>';
      print '</div>';
    ?>
  </div>
</div>
