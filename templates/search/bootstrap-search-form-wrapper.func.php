<?php

/**
 * @file
 * Stub file for bootstrap_bootstrap_search_form_wrapper().
 */

/**
 * Returns HTML for the Bootstrap search form wrapper.
 *
 * @ingroup theme_functions
 */
function bs_emc_bootstrap_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="reset" class="btn btn-default"><i class="fa fa-remove"><span class="sr-only">Close</span></i></button>';
  $output .= '<button type="submit" class="btn btn-default"><i class="fa fa-search"><span class="sr-only">Search</span></i></button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}
