<?php

/**
 * @file
 * Default theme implementation to display a Drupal front page.
 */
?>

<header class="header">
  <div class="header__logo-container">
    <a class="header__coe-link" href="https://www.engr.uky.edu/" title="University of Kentucky College of Engineering">
      <img class="header__logo header__logo--large" src="/sites/default/files/college-of-engineering_two-tone.png" alt="University of Kentucky College of Engineering">
      <img class="header__logo header__logo--small" src="/sites/default/files/uk-logo-blue.png" alt="University of Kentucky College of Engineering">
    </a>
    <h1 class="header__site-name"><a href="/" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></h1>
  </div> <!-- end .header__logo-container -->
  <button type="button" class="header__menu-toggle navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
    <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</header> <!-- end .header -->

<nav class="primary-nav">
  <div class="collapse navbar-collapse">
    <?php print render($primary_nav); ?>
  </div>
</nav> <!-- end .primary-nav -->

<?php if (!empty($page['home_slider'])): ?>
  <div class="lab-slider">
    <?php print render($page['home_slider']); ?>
  </div>
<?php endif; ?>


<?php if (!empty($page['highlighted'])): ?>
  <div class="lab-highlighted well"><?php print render($page['highlighted']); ?></div>
<?php endif; ?>
<div class="lab-messages">
  <?php print $messages; ?>
</div>

<?php if (!empty($tabs)): ?>
  <?php print render($tabs); ?>
<?php endif; ?>
<?php if (!empty($page['help'])): ?>
  <?php print render($page['help']); ?>
<?php endif; ?>
<?php if (!empty($action_links)): ?>
  <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>


<?php if (!empty($page['home_content'])): ?>
<div class="lab__content-wrapper">
  <div class="lab-content">
    <?php print render($page['home_content']); ?>
  </div>
  </div>
<?php endif; ?>


<?php if (!empty($page['home_news'])): ?>
  <div class="news">
    <h2>News</h2>
    <?php print render($page['home_news']); ?>
  </div>
<?php endif; ?>


<footer class="footer">
  <?php if (!empty($page['footer'])): ?>
    <div class="lab-footer">
      <?php print render($page['footer']); ?>
    </div>
  <?php endif; ?>
  <div class="coe-footer">
    <div class="coe-footer__wrapper">
      <div class="coe-footer__logo-container">
        <a href="https://www.engr.uky.edu/" title="University of Kentucky College of Engineering">
          <img class="coe-footer__logo" src="/sites/default/files/College_of_Engine-White.png" alt="University of Kentucky College of Engineering">
        </a>
      </div>
      <div class="coe-footer__global-footer">
        <span>&copy; <?php print date("Y"); ?> University of Kentucky</span>
        <span><a href="http://www.uky.edu/hr/employment/uk-is-equal-opportunity-employer" target="_blank">An Equal Opportunity University</a></span>
        <span><a href="http://www.uky.edu/accreditation/" target="_blank">Accreditation</a></span>
        <span><a href="https://directory.uky.edu/" target="_blank">Directory</a></span>
        <span><a href="mailto:webmaster@engr.uky.edu">Contact</a></span>
      </div>
    </div>

  </div>
</footer>
